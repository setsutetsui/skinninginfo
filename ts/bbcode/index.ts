import { ensuredSelector } from "../../typescript-lib/src/shared/common"
import { createElement } from "../../typescript-lib/src/shared/dom"
import { ImageMapData } from "../types/bbcode"
import { ImageMapArea } from "./components/ImageMapArea"
import { PostGenerator } from "./components/PostGenerator"

if (document.readyState === "loading") {
    // Loading hasn't finished yet
    document.addEventListener("DOMContentLoaded", main)
} else {
    // `DOMContentLoaded` has already fired
    main()
}

function main() {
    const root = ensuredSelector("#bbcode-editor-root")
    root.append(
        createElement("button", {
            attributes: {
                innerText: "create new imagemap",
                onclick: async () => {
                    const url = window.prompt("Please enter the image url.")
                    if (url == null || url == "") {
                        return
                    }
                    if(await validateImageSize(url)){
                        showGenerator([{ url: url, areas: [] }])
                    }
                },
            },
            className: "button button--primary",
        }),
        createElement("button", {
            attributes: {
                innerText: "import existing imagemap",
                onclick: () => {
                    const rawData = window.prompt("Please enter the data from your past imagemap.")?.trim()
                    if (rawData == null || rawData == "") {
                        return
                    }
                    showGenerator(parseData(rawData))
                },
            },
            className: "button button--primary",
        })
    )
}

function showGenerator(data: ImageMapData[]) {
    const root = ensuredSelector("#bbcode-editor-root")
    const generator = new PostGenerator("full", data)
    root.replaceWith(generator)
    // NEVER remove the line below, see finishInit for details
    generator.querySelectorAll<ImageMapArea>("image-map-area").forEach((e) => e.finishInit())
}

function parseData(data: string): ImageMapData[] {
    const regex = /\[img\]<--.*?-->(.*)\[\/img\]/gm

    const match = regex.exec(data)
    if (match == null) {
        throw new Error("invalid import data")
    }

    return JSON.parse(match[1]) as ImageMapData[]
}

async function validateImageSize(url: string): Promise<boolean> {
    let size = 0
    try {
        const response = await fetch(url, { method: "HEAD" })
        const contentHeader = response.headers.get("content-length")
        if (contentHeader != null) {
            size = parseInt(contentHeader)
        }
    } catch (e) {
        // most likely cors blocked
    }

    if(size == 0){
        window.alert("WARNING: Unable to check the size of the image. Please ensure it is 4MB or smaller")
        return true
    } else if ( size > 1024 * 1024){
        window.alert("WARNING: The image is bigger than 4MB and therefore will fail to load on the osu! website. Please try again with a smaller image")
        return false
    } else {
        return true
    }
}
