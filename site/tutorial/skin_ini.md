---
layout: tutorial-sidenav
highlight: tutorial
description: Everything there is to know about the skin.ini
title: Tutorial | skin.ini
---

# skin.ini

The skin.ini is the configuration file of your skin, it will determine how your skin behaves via `ConfigurationKey: value` pairs. For now we will only focus on a select few options and the overall structure and behaviour, other skin.ini options will be talked about in later parts of the tutorial. For a full list of skin.ini options please check the [skinnable files list]({{site.skinnable_files_list}}).

To create a skin.ini file make a file called `skin.txt` and rename it to `skin.ini` (make sure you have `view file extensions` enabled!) or copy a skin.ini from another case and delete its contents

## Structure

The skin.ini is made up of `[Sections]` which have `ConfigurationKey: value` pairs. This may sound complicated, but its actually quite simple, lets take a look at an example skin.ini:

```ini
[General]
Version: 2.5
Name: TestSkin
Author: RockRoller

[Colours]
Combo1: 255,255,255

[Fonts]
ComboPrefix: numbers/combo
//ComboOverlap: 80
```

### Sections

Sections are declared by putting them inside [square brackets] and in the example we can see three sections, General, Colours and Fonts. Sections bundle options together, but this is not for easier readability, an option will only work within the correct section. The skinnable files list contains all sections, all available options with a short description for each section and allowed values for each option.

### Options

Options are declared by writing down their key, followed by a colon (:), which gets followed by the value. There are many possible options for values, like numbers, number lists, path names, rgb(a) values and strings. If you do not use an option, or have an illegal value, osu! will use the default value for that option instead. If you completly forget to enter a value osu! will spit out an error and will also use the default value instead.
Make sure that you are only using each option **once**. If an option exists multiple times osu! will use the last one, which can lead to situation where you change the first occurance of an option and wonder why nothing changes when you reload your skin.

There are only three options we are going to look at in detail in this section, Name, Author and Version, all under the [General] section.

#### Version

This is arguably the most important skin.ini command, as it defines what version of the skinning system your skin uses. This does **not** define the release version of your skin, like `Test Skin V. Beta 02`. Please add release versions to the name of the skin.
Check the `skin versions` tab on the skinnable files sheet to see list of all valid values, as well as the differences between them, but you will most likely want to use `Version: 2.5` and it is what we will be using in this tutorial. Please be aware that you should **never** use `latest` for the skin.ini, as this may make the skin break when a new version of the skinning system gets released.

#### Name

This is the name you give your skin, this will be used as the file name when you hit "Export as .osk file" in the osu! client. Make sure to use a unique name, because when you import a skin while already having a skin installed with the same name it will overwrite the existing files, creating a messy mix of two skins. It is also good practise to add a release version identifier to your name, like `v1.0` or `v2.1` to prevent a new version of your skin overwriting an old version, not to mention it helps making your skin name unique.

#### Author

This is where your name goes, it shows who made this skin.

### Comments

If you look at the last line of the example you will see that there are two slashes `//` in front of the option. This means that its a comment and osu ignores it when reading the skin.ini. You can use this to temporarily disable an option without deleting it, or for leaving additional commentary. You could for example at the beginning of your skin.ini put a few comments containing all the colours you want to use within the skin.ini, so you wont have to always copy them from your editiong program. Just make sure to not overuse comments, as its important to keep your skin.ini somewhat clean, as it otherwise can get quite messy, especially when your skin.ini is using a lot of options.

```ini
// black: 0, 0, 0
// white: 255, 255, 255
```
