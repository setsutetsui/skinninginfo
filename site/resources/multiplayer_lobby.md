---
layout: master
highlight: resources
description: main menu template
title: Resources | Multiplayer Lobby Template
---

# Main Menu Template

{% include downloadable-image.html id="multiplayer-lobby-template" src="img/multiplayer_lobby/multiplayer_lobby_template.png"%}