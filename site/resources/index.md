---
layout: master
highlight: resources
description: A list of skinning resources
title: skinship | Resources
---

# Resources

Platforms

-   [Skinning Forum](https://osu.ppy.sh/community/forums/15)
-   [r/OsuSkins subreddit](https://www.reddit.com/r/OsuSkins/)
-   [skinship discord server](https://discord.skinship.xyz/)

Skin Collections

-   [Completed Skins Compendium](https://compendium.skinship.xyz/)
-   [Circle People Pro Player Skin Collection](https://circle-people.com/skins/)

Programs and Tools

-   [ImageMap Editor](/resources/post-generator)
-   [Image Scaler](https://osu.ppy.sh/community/forums/topics/762684)
-   [Fringe Remover](https://osu.ppy.sh/community/forums/topics/1244643)
-   [Skin Checker](https://osu.ppy.sh/community/forums/topics/617168)
-   [Mania Column Centering](https://osu.ppy.sh/community/forums/topics/581972)
-   [pinga](https://css-ig.net/)
-   [Bulk Rename Utility](https://www.bulkrenameutility.co.uk/)
-   [Advanced Renamer](https://www.advancedrenamer.com/)
-   [waifu2x](https://github.com/nagadomi/waifu2x)

Miscellaneous

-   [Skinnable Files List](https://osu.ppy.sh/community/forums/topics/186787)
-   [Completed Skins Queue](https://osu.ppy.sh/community/forums/topics/686672)
-   [Default Skin](http://www.mediafire.com/file/3fvcpl61wnz3xfc/osu%2521_Default_skin_template.osk/file)
-   [Japanese Text on the Default Skin](./japanese_text_on_the_default_skin)

Templates

-   [Song Selection Template](./song_selection)
-   [Main Menu Template](./main_menu)
-   [Main Menu Detailed Template](./main_menu_detailed)
-   [Multiplayer Lobby Template](./multiplayer_lobby)
-   [Galvit's Template Skin](https://osu.ppy.sh/community/forums/topics/923143)
-   [XetThe's Slate (open source osu! skin template)](https://github.com/xetThe/osu-slate)
-   [skin.ini Template](/resources/skin.ini)