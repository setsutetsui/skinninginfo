'use strict';

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

const arrow = `<svg viewBox="0 0 24 24" fill="none"><path d="M9 5L16 12L9 19" fill="#1c1719" stroke="#1c1719" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>`;
const at = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M14.243 5.757a6 6 0 10-.986 9.284 1 1 0 111.087 1.678A8 8 0 1118 10a3 3 0 01-4.8 2.401A4 4 0 1114 10a1 1 0 102 0c0-1.537-.586-3.07-1.757-4.243zM12 10a2 2 0 10-4 0 2 2 0 004 0z" clip-rule="evenodd"></path></svg>`;
const avatar = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;
const bookmark = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"></path></svg>`;
const bookmarkFilled = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path d="M5 4a2 2 0 012-2h6a2 2 0 012 2v14l-5-2.5L5 18V4z"></path></svg>`;
const change = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4"></path></svg>`;
const chat = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"></path></svg>`;
const close = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>`;
const collection = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"></path></svg>`;
const copy = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 16H6a2 2 0 01-2-2V6a2 2 0 012-2h8a2 2 0 012 2v2m-6 12h8a2 2 0 002-2v-8a2 2 0 00-2-2h-8a2 2 0 00-2 2v8a2 2 0 002 2z"></path></svg>`;
const documentAdd = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>`;
const documentFilled = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" ><path fill-rule="evenodd" d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4z" clip-rule="evenodd"></path></svg>`;
const download = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"></path></svg>`;
const externalLink = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>`;
const image = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>`;
const message = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"></path></svg>`;
const refresh = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg>`;
const search = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>`;
const setting = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>`;
const trash = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>`;
const warning = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd"></path></svg>`;
const chevronDown = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path></svg>`;
const edit = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" ><path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z"></path><path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd"></path></svg>`;
const upload = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"></path></svg>`;
const pause = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 9v6m4-6v6m7-3a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;
const play = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14.752 11.168l-3.197-2.132A1 1 0 0010 9.87v4.263a1 1 0 001.555.832l3.197-2.132a1 1 0 000-1.664z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;
const info = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;

var icons = /*#__PURE__*/Object.freeze({
    __proto__: null,
    arrow: arrow,
    at: at,
    avatar: avatar,
    bookmark: bookmark,
    bookmarkFilled: bookmarkFilled,
    change: change,
    chat: chat,
    close: close,
    collection: collection,
    copy: copy,
    documentAdd: documentAdd,
    documentFilled: documentFilled,
    download: download,
    externalLink: externalLink,
    image: image,
    message: message,
    refresh: refresh,
    search: search,
    setting: setting,
    trash: trash,
    warning: warning,
    chevronDown: chevronDown,
    edit: edit,
    upload: upload,
    pause: pause,
    play: play,
    info: info
});

class MissingElementException extends Error {
    constructor(message) {
        super(message);
        this.name = "MissingElementException";
    }
}

function createElementFromHTMLString(html) {
    return createElement("div", {
        attributes: {
            innerHTML: html,
        },
    }).firstChild;
}
function createElement(tagName, options = {}) {
    const { attributes = {}, style = {}, className = "", children = [], events = [] } = options;
    const element = document.createElement(tagName);
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: `rr-lib ${className}` }));
    Object.assign(element.style, style);
    element.append(...children);
    events.forEach((event) => element.addEventListener(event.type, event.handler));
    return element;
}
function createIcon(name, options = {}) {
    const { size = 16 } = options, rest = __rest(options, ["size"]);
    const element = createElementFromHTMLString(icons[name]);
    Object.assign(element.style, Object.assign({ width: `${size}px`, height: `${size}px` }, rest));
    return element;
}

function wait(ms) {
    return new Promise((r) => setTimeout(r, ms));
}
function ensuredSelector(query, parent = document) {
    const element = parent.querySelector(query);
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`);
    }
    return element;
}

class BBCodeData {
}
class TextAreaData extends BBCodeData {
    constructor(value) {
        super();
        this.value = value;
    }
}
class ListRowData extends TextAreaData {
    constructor(value) {
        super(value);
    }
}
class ListData extends BBCodeData {
    constructor(type, items) {
        super();
        this.type = type;
        this.items = items;
    }
}
class ImageMapData extends BBCodeData {
    constructor(url, areas) {
        super();
        this.url = url;
        this.areas = areas;
    }
}
class ImageMapAreaData extends BBCodeData {
    constructor(x, y, width, height, title, url) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.title = title;
        this.url = url;
    }
}

class ImageMapAreaForm extends HTMLElement {
    constructor(parent, data) {
        super();
        this.tooltipElement = createElement("input", {
            attributes: {
                placeholder: "tooltip (optional)",
                title: "tooltip (optional)",
                type: "text",
                value: "",
            },
        });
        this.urlElement = createElement("input", {
            attributes: {
                placeholder: "url (optional)",
                title: "url (optional)",
                type: "text",
                value: "",
            },
        });
        this.xElement = this.createInputField();
        this.yElement = this.createInputField();
        this.wElement = this.createInputField();
        this.hElement = this.createInputField();
        this.classList.add("image-map-editor__area-form");
        if (data) {
            this.urlElement.value = data.url;
            this.tooltipElement.value = data.title;
        }
        this.append(this.tooltipElement, this.urlElement, createElement("button", {
            className: "image-map-editor__area-form-button image-map-editor__area-form-button--delete button",
            attributes: {
                innerText: "delete",
                onclick: () => {
                    parent.remove();
                    this.remove();
                },
            },
        }), createElement("button", {
            className: "image-map-editor__area-form-button image-map-editor__area-form-button--width button button--primary",
            attributes: {
                innerText: "← full width →",
                onclick: () => {
                    parent.setWidth(100.0);
                },
            },
        }), createElement("button", {
            className: "image-map-editor__area-form-button image-map-editor__area-form-button--height button button--primary",
            attributes: {
                innerText: "↕ full height ↕",
                onclick: () => {
                    parent.setHeight(100.0);
                },
            },
        }), this.createField(this.xElement, "x"), this.createField(this.yElement, "y"), this.createField(this.wElement, "width"), this.createField(this.hElement, "height"));
    }
    getXField() {
        return this.xElement;
    }
    getYField() {
        return this.yElement;
    }
    getWidthField() {
        return this.wElement;
    }
    getHeightField() {
        return this.hElement;
    }
    createInputField() {
        return createElement("input", {
            attributes: {
                type: "number",
                value: "0.0",
                max: "100.0",
                min: "-100.0",
                step: "0.1",
            },
        });
    }
    createField(input, label) {
        return createElement("label", {
            children: [input],
            attributes: {
                innerText: label,
            },
        });
    }
    getTitle() {
        return this.tooltipElement.value;
    }
    getURL() {
        const value = this.urlElement.value.trim();
        if (value == "") {
            return "";
        }
        let urlObj;
        try {
            urlObj = new URL(value);
        }
        catch (e) {
            try {
                urlObj = new URL("https://" + value);
            }
            catch (e) {
                urlObj = "";
            }
        }
        return urlObj.toString();
    }
}
customElements.define("imagemap-editor-area-controls", ImageMapAreaForm);

class ImageMapArea extends HTMLElement {
    constructor(parent, e, data) {
        super();
        this.y = 0;
        this.x = 0;
        this.calcWidth = 0;
        this.calcHeight = 0;
        this.prevX = 0;
        this.prevY = 0;
        this.initFinished = false;
        this.parent = parent;
        this.classList.add("image-map-editor__area");
        this.form = new ImageMapAreaForm(this, data);
        this.xElement = this.form.getXField();
        this.yElement = this.form.getYField();
        this.wElement = this.form.getWidthField();
        this.hElement = this.form.getHeightField();
        if (data) {
            this.setXY(data.x, data.y);
            this.setWH(data.width, data.height);
        }
        this.xElement.addEventListener("change", () => {
            this.setXY(parseFloat(this.xElement.value), this.y);
        });
        this.yElement.addEventListener("change", () => {
            this.setXY(this.x, parseFloat(this.yElement.value));
        });
        this.wElement.addEventListener("change", () => {
            this.setWH(parseFloat(this.wElement.value), this.calcHeight);
        });
        this.hElement.addEventListener("change", () => {
            this.setWH(this.calcWidth, parseFloat(this.hElement.value));
        });
        if (e != null) {
            this.setXY(this.getCoordinateAsPercentage(e.clientX, "x"), this.getCoordinateAsPercentage(e.clientY, "y"));
        }
        let controlMenuButton = createElement("div", {
            className: "image-map-editor__area-settings-button",
            attributes: {
                innerText: "",
                onclick: () => {
                    this.form.classList.toggle("visible");
                },
                title: "settings",
            },
            children: [createIcon("setting")],
        });
        let dragableArea = createElement("div", {
            className: "image-map-editor__area-dragable-area",
        });
        this.append(createElement("div", {
            className: "image-map-editor__area-wrapper",
            children: [
                controlMenuButton,
                dragableArea,
                createElement("div", {
                    className: "image-map-editor__area-resize-handle",
                    attributes: {
                        title: "resize",
                    },
                }),
            ],
        }));
        parent.append(this.form);
        parent.addEventListener("click", (e) => {
            const target = e.target;
            if (target == null) {
                return;
            }
            if (!(this.form.contains(e.target) || this.contains(e.target))) {
                this.form.classList.remove("visible");
            }
        });
        this.tabIndex = 0;
        this.addEventListener("keydown", (e) => {
            if (e.key == "Delete") {
                if (window.confirm("Delete this area?")) {
                    this.remove();
                    this.form.remove();
                }
            }
        });
        let mouseDown = false;
        dragableArea.addEventListener("mousedown", (e) => {
            this.prevX = 0;
            this.prevY = 0;
            if (e.target != dragableArea) {
                mouseDown = false;
                return;
            }
            mouseDown = true;
        });
        dragableArea.addEventListener("mousemove", (e) => {
            if (mouseDown) {
                this.move(e);
            }
        });
        dragableArea.addEventListener("mouseup", (e) => {
            mouseDown = false;
        });
        dragableArea.addEventListener("mouseleave", (e) => {
            mouseDown = false;
        });
        new ResizeObserver(() => {
            this.fetchSize();
            this.style.width = "";
            this.style.height = "";
        }).observe(this);
    }
    getBBCodeData() {
        return new ImageMapAreaData(this.x, this.y, this.calcWidth, this.calcHeight, this.form.getTitle().trim(), this.form.getURL().trim());
    }
    setWidth(newWidth) {
        this.setXY(0.0, this.y);
        this.setWH(newWidth, this.calcHeight);
    }
    setHeight(newHeight) {
        this.setXY(this.x, 0.0);
        this.setWH(this.calcWidth, newHeight);
    }
    move(e) {
        let thisX = this.getCoordinateAsPercentage(e.clientX, "x");
        let thisY = this.getCoordinateAsPercentage(e.clientY, "y");
        if (this.prevY == 0 || this.prevX == 0) {
            this.prevX = thisX;
            this.prevY = thisY;
        }
        else {
            let horizontalDistance = this.prevX - thisX;
            let verticalDistance = this.prevY - thisY;
            this.setXY(this.x - horizontalDistance, this.y - verticalDistance);
            this.prevX = thisX;
            this.prevY = thisY;
        }
        if (this.prevX > 100.0) {
            this.prevX = 100.0;
        }
        if (this.prevY > 100.0) {
            this.prevY = 100.0;
        }
    }
    expand(e) {
        let thisX = this.getCoordinateAsPercentage(e.clientX, "x");
        let thisY = this.getCoordinateAsPercentage(e.clientY, "y");
        if (thisX < this.x) {
            thisX = this.x;
        }
        if (thisY < this.y) {
            thisY = this.y;
        }
        let width = thisX - this.x;
        let height = thisY - this.y;
        this.setWH(width, height);
    }
    fetchSize() {
        if (this.initFinished) {
            const parentRect = this.parent.getBoundingClientRect();
            const thisRect = this.getBoundingClientRect();
            this.setWH((thisRect.width / parentRect.width) * 100, (thisRect.height / parentRect.height) * 100);
        }
    }
    finishInit() {
        this.initFinished = true;
    }
    setXY(x, y) {
        x = this.round(x);
        y = this.round(y);
        if (x < 0) {
            x = 0;
        }
        if (y < 0) {
            y = 0;
        }
        if (x + Math.abs(this.calcWidth) > 100.0) {
            x = this.round(100.0 - Math.abs(this.calcWidth));
        }
        if (y + Math.abs(this.calcHeight) > 100.0) {
            y = this.round(100.0 - Math.abs(this.calcHeight));
        }
        this.y = y;
        this.x = x;
        this.style.setProperty("--area-x", this.x.toString() + "%");
        this.style.setProperty("--area-y", this.y.toString() + "%");
        this.form.style.setProperty("--area-x", this.x.toString() + "%");
        this.form.style.setProperty("--area-y", this.y.toString() + "%");
        this.xElement.value = this.x.toString();
        this.yElement.value = this.y.toString();
    }
    setWH(width, height) {
        width = this.round(width);
        height = this.round(height);
        if (width + this.x > 100.0) {
            width = this.round(100.0 - this.x);
        }
        if (height + this.y > 100.0) {
            height = this.round(100.0 - this.y);
        }
        this.style.setProperty("--area-width", width.toString() + "%");
        this.style.setProperty("--area-height", height.toString() + "%");
        this.form.style.setProperty("--area-width", width.toString() + "%");
        this.form.style.setProperty("--area-height", height.toString() + "%");
        this.calcWidth = width;
        this.calcHeight = height;
        this.wElement.value = this.calcWidth.toString();
        this.hElement.value = this.calcHeight.toString();
    }
    getCoordinateAsPercentage(coord, type) {
        const boundingRect = this.parent.getBoundingClientRect();
        let offset = type == "x" ? boundingRect.left : boundingRect.top;
        let size = type == "x" ? boundingRect.width : boundingRect.height;
        return this.round(((coord - offset) / size) * 100);
    }
    round(value) {
        return parseFloat(value.toFixed(1));
    }
}
customElements.define("imagemap-editor-area", ImageMapArea);

class ImageMapEditor extends HTMLElement {
    constructor(data) {
        super();
        this.imgElement = createElement("img", {
            className: "image-map-editor__image",
            attributes: {
                draggable: false,
            },
        });
        this.currentArea = null;
        this.url = data.url;
        this.imgElement.src = this.url;
        data.areas.forEach((area) => {
            this.append(new ImageMapArea(this, null, area));
        });
        this.classList.add("image-map-editor");
        this.append(this.imgElement);
        let mouseDown = false;
        this.addEventListener("mousedown", (e) => {
            if (e.target != this.imgElement) {
                mouseDown = false;
                return;
            }
            if (document.querySelector(".image-map-editor__area-form.visible") != null) {
                return;
            }
            mouseDown = true;
            this.currentArea = new ImageMapArea(this, e);
            this.append(this.currentArea);
            this.currentArea.finishInit();
        });
        this.addEventListener("mousemove", (e) => {
            var _a;
            if (mouseDown) {
                (_a = this.currentArea) === null || _a === void 0 ? void 0 : _a.expand(e);
            }
        });
        this.addEventListener("mouseup", (e) => {
            var _a;
            mouseDown = false;
            (_a = this.currentArea) === null || _a === void 0 ? void 0 : _a.classList.add("placement-finished");
        });
        this.addEventListener("mouseleave", (e) => {
            var _a;
            mouseDown = false;
            (_a = this.currentArea) === null || _a === void 0 ? void 0 : _a.classList.add("placement-finished");
        });
    }
    getBBCodeData() {
        const data = new ImageMapData(this.url.trim(), []);
        this.querySelectorAll(".image-map-editor__area").forEach((area) => {
            data.areas.push(area.getBBCodeData());
        });
        return data;
    }
}
customElements.define("imagemap-editor", ImageMapEditor);

class PostGenerator extends HTMLElement {
    constructor(mode, data) {
        super();
        this.classList.add("post-generator", `post-generator--${mode}`);
        const exportButton = createElement("button", {
            attributes: {
                onclick: () => __awaiter(this, void 0, void 0, function* () {
                    navigator.clipboard.writeText(this.getBBCode());
                    const previousText = exportButton.innerText;
                    exportButton.innerText = "copied to clipboard";
                    yield wait(2000);
                    exportButton.innerText = previousText;
                }),
                innerText: "get bbcode",
            },
            className: "button button--primary",
        });
        this.append(new ImageMapEditor(data[0]), exportButton);
    }
    getBBCode() {
        const data = [];
        this.querySelectorAll("." + this.className.split(" ").join(".") + " > .bbcode-element-wrapper").forEach((e) => data.push(e.getChild().getBBCodeData()));
        this.querySelectorAll("." + this.className.split(" ").join(".") + " > .image-map-editor").forEach((e) => data.push(e.getBBCodeData()));
        let bbCode = "";
        data.forEach((entry) => {
            if (entry instanceof ImageMapData) {
                bbCode += this.parseImageMapData(entry) + "\n";
            }
            else if (entry instanceof ListData) {
                bbCode += this.parseListData(entry) + "\n";
            }
            else if (entry instanceof TextAreaData) {
                bbCode += this.parseTextAreaData(entry) + "\n";
            }
        });
        bbCode += `\n[img]<-- WARNING: anything within this img tag is data used to read in your generated bbcode back into the post generator. Please do not touch this. If you wish to import this please copy everything between the opening and closing tag, including the tags themself-->${JSON.stringify(data)}[/img]`;
        return bbCode;
    }
    parseImageMapData(data) {
        const validAreas = data.areas.filter((area) => {
            return !(area.url == "" && area.title == "");
        });
        if (validAreas.length == 0) {
            return "";
        }
        let bbCode = "[imagemap]\n";
        bbCode += data.url + "\n";
        validAreas.forEach((area) => {
            bbCode += `${area.x} ${area.y} ${Math.abs(area.width)} ${Math.abs(area.height)} ${area.url == "" ? "#" : area.url} ${area.title}\n`;
        });
        bbCode += "[/imagemap]";
        return bbCode;
    }
    parseListData(data) {
        if (data.items.length == 0) {
            return "";
        }
        const childBBCodes = [];
        data.items.forEach((entry) => {
            if (entry instanceof ListData) {
                childBBCodes.push(this.parseListData(entry));
            }
            else if (entry instanceof ListRowData) {
                childBBCodes.push(this.parseTextAreaData(entry));
            }
        });
        const validChildBBCodes = childBBCodes.filter((code) => code != "");
        if (validChildBBCodes.length == 0) {
            return "";
        }
        let bbCode = data.type == "numbered" ? "[list=1]\n" : "[list]\n";
        validChildBBCodes.forEach((entry) => {
            bbCode += entry + "\n";
        });
        bbCode += "[/list]";
        return bbCode;
    }
    parseTextAreaData(data) {
        return data.value;
    }
}
customElements.define("post-generator", PostGenerator);

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
}
else {
    main();
}
function main() {
    const root = ensuredSelector("#bbcode-editor-root");
    root.append(createElement("button", {
        attributes: {
            innerText: "create new imagemap",
            onclick: () => __awaiter(this, void 0, void 0, function* () {
                const url = window.prompt("Please enter the image url.");
                if (url == null || url == "") {
                    return;
                }
                if (yield validateImageSize(url)) {
                    showGenerator([{ url: url, areas: [] }]);
                }
            }),
        },
        className: "button button--primary",
    }), createElement("button", {
        attributes: {
            innerText: "import existing imagemap",
            onclick: () => {
                var _a;
                const rawData = (_a = window.prompt("Please enter the data from your past imagemap.")) === null || _a === void 0 ? void 0 : _a.trim();
                if (rawData == null || rawData == "") {
                    return;
                }
                showGenerator(parseData(rawData));
            },
        },
        className: "button button--primary",
    }));
}
function showGenerator(data) {
    const root = ensuredSelector("#bbcode-editor-root");
    const generator = new PostGenerator("full", data);
    root.replaceWith(generator);
    generator.querySelectorAll("image-map-area").forEach((e) => e.finishInit());
}
function parseData(data) {
    const regex = /\[img\]<--.*?-->(.*)\[\/img\]/gm;
    const match = regex.exec(data);
    if (match == null) {
        throw new Error("invalid import data");
    }
    return JSON.parse(match[1]);
}
function validateImageSize(url) {
    return __awaiter(this, void 0, void 0, function* () {
        let size = 0;
        try {
            const response = yield fetch(url, { method: "HEAD" });
            const contentHeader = response.headers.get("content-length");
            if (contentHeader != null) {
                size = parseInt(contentHeader);
            }
        }
        catch (e) {
        }
        if (size == 0) {
            window.alert("WARNING: Unable to check the size of the image. Please ensure it is 4MB or smaller");
            return true;
        }
        else if (size > 1024 * 1024) {
            window.alert("WARNING: The image is bigger than 4MB and therefore will fail to load on the osu! website. Please try again with a smaller image");
            return false;
        }
        else {
            return true;
        }
    });
}
