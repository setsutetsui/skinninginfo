---
layout: master
highlight: guides
title: Guides | Centring Accuracy
description: This guide teaches you how to centre the accuracy on the playfield.
---

# Centring Accuracy

This guide will teach you how to centre the accuracy counter on the playfield.

Note that by doing this, the percentage symbol after the accuracy will disappear. This is due to osu! stopping to load the texture and loading only the hitbox, due to its size. Also note that this will work in all game modes, but will look off in osu!taiko as the accuracy counter is shifted downwards due to the taiko scoring animation.

Knowledge of HD and SD files is needed. If you do not know the difference between these, or how they work, please read [this part](https://skinship.xyz/tutorial/introduction#hdsd-elements-aspect-ratios-and-resolution) of the skinning tutorial.

## Roughly centring accuracy

This part of the guide will teach you how to roughly centre the accuracy counter - i.e. centring it 'good enough'. To learn how to perfectly centre accuracy, look at the `Perfectly centring accuracy` part of this guide.

1.  Open your skin's folder. The folder can be found by navigating to the `Skins\` directory of your osu! installation. By default this will be located at `C:\Users\[User Name]\AppData\Local\osu!\Skins\`. You can locate this by pressing `CTRL + O` in the osu! window to bring up settings, clicking `Open osu! folder`, and navigating to the `Skins\` directory. Alternatively, you can go to settings, navigate to the `SKIN` section, and click `Open current skin folder`.
2.  Find your osu! resolution's **aspect ratio**. You can find this by opening up osu! settings, navigating to the `GRAPHICS` section, find the `Resolution:` setting and look up on the internet what aspect ratio this resolution corresponds to.
3.  Find the SD skinning width (ratio_width in the following formula) of your aspect ratio

    | ratio | width  |
    | ----- | ------ |
    | 4:3   | 1024px |
    | 16:9  | 1366px |
    | 16:10 | 1280px |
    | 21:9  | 1720px |

4.  Use the following formula to find the canvas width that your `score-percent.png` file will need to be:
    <img src="/guides/img/rough.jpg">
    Where:<br>
    `a` is the ratio_width you got from step 3<br>
5.  Using an image editor, change the canvas width of `score-percent.png` to the number you got from step 4.
6.  Reload your skin using `CTRL + ALT + SHIFT + S`. Your accuracy will now be displayed in the centre of the playfield.

## Perfectly centring accuracy

This part of the guide will teach you how to perfectly centre the accuracy counter.

1. Open your skin's folder. The folder can be found by navigating to the `Skins\` directory of your osu! installation. By default this will be located at `C:\Users\[User Name]\AppData\Local\osu!\Skins\`. You can locate this by pressing `CTRL + O` in the osu! window to bring up settings, clicking `Open osu! folder`, and navigating to the `Skins\` directory. Alternatively, you can go to settings, navigate to the `SKIN` section, and click `Open current skin folder`.
2. Find your osu! resolution's **aspect ratio**. You can find this by opening up osu! settings, navigating to the `GRAPHICS` section, find the `Resolution:` setting and look up on the internet what aspect ratio this resolution corresponds to.
3. Find the SD skinning width (ratio_width in the following formula) of your aspect ratio

    | ratio | width  |
    | ----- | ------ |
    | 4:3   | 1024px |
    | 16:9  | 1366px |
    | 16:10 | 1280px |
    | 21:9  | 1720px |

4. Find the width of your `score-0.png` file. All of your score number files should be the same resolution. Also find the width of your `score-dot.png` or `score-comma.png` if your locale uses commas for decimals.
5. Find your score number overlap. This is located inside the skin.ini at the `ScoreOverlap:` command under `[Fonts]`. If it is not specified, it will be `-2` by default.
6. Open your `score-percent.png` file in an image editor and use the following formula to find the width which the file will need to be:
   <img src="/guides/img/exact.jpg">
   Where:<br>
   `a` is the ratio_width you got from step 3<br>
   `b` is the width of `score-dot` or `score-comma`<br>
   `c` is the width of `score-0.png`<br>
   `d` is the value of `ScoreOverlap:` in the skin.ini
7. Change the canvas width of `score-percent.png` to the number you got from step 6.
8. Reload your skin using `CTRL + ALT + SHIFT + S`. Your accuracy will now be displayed in the centre of the playfield.
