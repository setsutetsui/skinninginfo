---
layout: master
highlight: guides
description: This guide teaches you how to extract skin elements and skin.ini data from beatmaps.
title: Guides | Extracting Beatmap Skins
---

# Extracting Skins from Beatmaps

This guide will teach you how to extract skin assets from beatmaps which have custom skin elements specified. Custom elements may be specified in two ways: By adding the skin element to the beatmap's folder, or by specifying skin.ini data in the .osu file. As such, this guide will be split up into two parts.

## Prerequisite steps

1. Press `Ctrl + O` to bring up the settings menu.
2. Navigate down to `SKIN` section and click on the `Open current skin folder` button.
3. Navigate back to the beatmap with the skin elements you want to extract, right click on it, and click on `5. Edit` to open it in the editor.
4. On the top menu bar, click on `File -> Open Song Folder`.

## Extracting skin elements

1. Select the skin elements in the beatmap's folder that you wish to copy.
2. Paste them into the folder of your current skin which you opened in `Prerequisite steps`, and replace the already-existing elements (backup as necessary).
3. Return to osu! and press `Ctrl + Alt + Shift + S` to reload your skin and your beatmap skin elements should now be part of your skin.

## Extracting skin.ini data

1. Open the skin.ini file within the current skin folder which you opened in `Prerequisite steps` in a text editor. VSCode is recommended but the default text editor on your system will suffice.
2. Open the .osu file of the beatmap (of the specific difficulty) in a text editor.
3. Navigate to the `[Colours]` section in the .osu file, which is located between the `[TimingPoints]` and `[HitObjects]` section.
4. Copy and replace the desired data into the `[Colours]` section of the skin.ini file (backup as necessary).
5. Return to osu! and press `Ctrl + Alt + Shift + S` to reload your skin and your beatmap skin.ini data should now be part of your skin.
